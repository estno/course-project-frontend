export const COURSE_URL = "http://localhost:8000/api/courses/"

export function getCourseUrl(urlAppendix?: string) {
    return `${COURSE_URL}/api/courses/${urlAppendix}`
}
