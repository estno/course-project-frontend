import { z } from "zod"

export const courseSchema = z.object({
  id: z.string(),
  name: z.string(),
  description: z.string(),
  studyLoad: z.number(),
  level: z.string(),
  startDate: z.string(),
  endDate: z.string(),
  coordinators: z.string(),
  courseLengthInDays: z.number(),
})

export type Course = z.infer<typeof courseSchema>
