"use client"

import { DotsHorizontalIcon } from "@radix-ui/react-icons"
import { Row } from "@tanstack/react-table"

import { Button } from "@/components/ui/button"
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"

import { courseSchema } from "../data/coursesSchema"
import Link from "next/link"
import axios from "axios"
import { COURSE_URL } from "@/lib/url"

interface DataTableRowActionsProps<TData> {
  row: Row<TData>
}

export function DataTableRowActions<TData>({
  row,
}: DataTableRowActionsProps<TData>) {
    const task = courseSchema.parse(row.original)

    const onDeleteClick = async () => {
        if (!confirm("Kas oled kindel?")) return
        axios.delete(`${COURSE_URL}${task.id}`)
            .then(response => {
                if (response.status === 200) {
                    console.log(`Deleted post with ID ${task.id}`);
                    window.location.reload()
                }
            })
            .catch(error => {
                console.error(error);
            });
    }

    return (
        <DropdownMenu>
            <DropdownMenuTrigger asChild>
                <Button
                    variant="ghost"
                    className="flex h-8 w-8 p-0 data-[state=open]:bg-muted"
                >
                    <DotsHorizontalIcon className="h-4 w-4" />
                    <span className="sr-only">Open menu</span>
                </Button>
            </DropdownMenuTrigger>
            <DropdownMenuContent align="end" className="w-[160px]">
                <Link href={`/course/${task.id}`} className="cursor-pointer">
                    <DropdownMenuItem>
                        Muuda
                    </DropdownMenuItem>
                </Link>
                <Link href={"#delete"} onClick={onDeleteClick}>
                    <DropdownMenuItem>
                        Kustuta
                    </DropdownMenuItem>
                </Link>
            </DropdownMenuContent>
        </DropdownMenu>
    )
}
