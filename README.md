Course API - NextJs

## Getting Started

First, install node_modules packages:

```bash
npm install
# or
npm i
```

Run development server

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

or

## Docker Development server

```bash
docker-compose up -d --build
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
