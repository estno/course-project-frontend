# Use the official Node.js image
FROM node:18

# Set working directory
WORKDIR /app

# Install dependencies
COPY package.json .
RUN npm install

# Copy application files
COPY . .

# Expose port 3000 and start Next.js server
EXPOSE 3000
#CMD ["npm run dev"]
CMD npm run dev
