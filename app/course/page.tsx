
'use client'

import Link from "next/link"
import { useEffect, useState } from "react"

import { z } from "zod"
import axios from "axios"

import { columns } from "@/components/course/components/columns"
import { DataTable } from "@/components/course/components/data-table"
import { courseSchema } from "@/components/course/data/coursesSchema"
import { Button } from "@/components/ui/button"
import { COURSE_URL } from "@/lib/url"

import { defaultHeader } from "@/lib/utils"

type KK = {
    id: string;
    name: string;
    description: string;
    studyLoad: number;
    level: string;
    startDate: string;
    endDate: string;
    coordinators: string;
    courseLengthInDays: number;
}

const CoursePage = () => {

    const [courses, setCourses] = useState<KK[]>()

    useEffect(() => {
        getCourses()
        return () => {};
    }, [])

    const getCourses = async () => {

        const response = await axios({
            method: "get",
            headers: defaultHeader,
            url: `${COURSE_URL}`,
        }).then(response => response).catch(err => console.log(err))
        if (response?.status !== 200) return;
        const ert = z.array(courseSchema).parse(response.data.courses)
        setCourses(prev => prev = ert)
    }

    return (
        <>
            <div className="h-full flex-1 flex-col md:flex min-w-30">
                <div className="flex flex-col gap-4 justify-between space-y-2">
                    <div className="flex flex-col gap-4">
                        <div className="flex justify-between">
                            <div>
                                <h2 className="text-2xl font-bold tracking-tight">Kursused</h2>
                            </div>
                            <div>
                                <Button variant={"default"} asChild>
                                    <Link href={"/course/create"}>Lisa uus kursus</Link>
                                </Button>
                            </div>
                        </div>
                    </div>
                    {courses ? <DataTable data={courses} columns={columns} /> : <>Laadimine...</>}
                </div>

            </div>
        </>
    )
}

export default CoursePage
