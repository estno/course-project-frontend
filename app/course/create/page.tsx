'use client'

import React from 'react'
import axios from 'axios';
import { z } from "zod"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"

import { useToast } from "@/components/ui/use-toast"
import { cn, defaultHeader } from "@/lib/utils"
import { CalendarIcon } from "@radix-ui/react-icons"
import { Calendar } from "@/components/ui/calendar"
import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from '@/components/ui/input'
import {
    Popover,
    PopoverContent,
    PopoverTrigger,
  } from "@/components/ui/popover"
import { COURSE_URL } from '@/lib/url';

const formSchema = z.object({
  name: z.string().min(1).max(200),
  description: z.string().max(2000),
  studyLoad: z.string().max(30),
  level: z.string(),
  courseLengthInDays: z.string().max(365),
  startDate: z.date(),
  coordinators: z.string(),
})

const CourseCreatePage = () => {

    const { toast } = useToast()

    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema),
        defaultValues: {
            name: "",
            studyLoad: "",
            level: "",
            coordinators: "",
            courseLengthInDays: ""
        },
    })

    const onSubmit = (values: z.infer<typeof formSchema>) => {
        axios({
            method: "post",
            headers: defaultHeader,
            url: `${COURSE_URL}`,
            data: {
                ...values,
                studyLoad: parseInt(values.studyLoad),
                courseLengthInDays: parseInt(values.courseLengthInDays),
                description: values.description === "" ? null : values.description
            }
        })
        .then(response => {
            if (response.status === 200) {
                form.reset()
                toast({
                    title: "Kursus edukalt lisatud",
                })
            }
        }).catch(err => {
            toast({
                title: "Lisamisel tekkis viga",
            })
        })
    }

    return (
        <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
                <FormField
                    control={form.control}
                    name="name"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Nimi</FormLabel>
                            <FormControl>
                                <Input required {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    control={form.control}
                    name="description"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Kirjeldus</FormLabel>
                            <FormControl>
                                <Input {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    control={form.control}
                    name="studyLoad"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Õppemaht</FormLabel>
                            <FormControl>
                                <Input required {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    control={form.control}
                    name="level"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Õppetase</FormLabel>
                            <FormControl>
                                <Input required placeholder='bachelor, master, doctoral' {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    control={form.control}
                    name="courseLengthInDays"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Kursuse pikkus päevades</FormLabel>
                            <FormControl>
                                <Input required {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    control={form.control}
                    name="startDate"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel className='mr-6'>Alguskuupäev</FormLabel>
                            <FormControl>
                                <Popover>
                                    <PopoverTrigger asChild>
                                        <FormControl>
                                            <Button
                                            variant={"outline"}
                                            className={cn(
                                                "w-[240px] pl-3 text-left font-normal",
                                                !field.value && "text-muted-foreground"
                                            )}
                                            >
                                            {field.value ? (
                                                field.value.toLocaleDateString('et')
                                            ) : (
                                                <span>Pick a date</span>
                                            )}
                                            <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                                            </Button>
                                        </FormControl>
                                    </PopoverTrigger>
                                    <PopoverContent className="w-auto p-0" align="start">
                                        <Calendar
                                            mode="single"
                                            selected={field.value}
                                            onSelect={field.onChange}
                                            disabled={(date) =>
                                                date < new Date() || date < new Date("1900-01-01")
                                            }
                                            initialFocus
                                        />
                                    </PopoverContent>
                                </Popover>
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    control={form.control}
                    name="coordinators"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Õppejõud</FormLabel>
                            <FormControl>
                                <Input required {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <Button type="submit">Salvesta</Button>
            </form>
        </Form>
    )
}

export default CourseCreatePage
