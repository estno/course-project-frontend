'use client'

import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { z } from "zod"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"

import { useToast } from "@/components/ui/use-toast"
import { cn, defaultHeader } from "@/lib/utils"
import { CalendarIcon } from "@radix-ui/react-icons"
import { Calendar } from "@/components/ui/calendar"
import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from '@/components/ui/input'
import {
    Popover,
    PopoverContent,
    PopoverTrigger,
  } from "@/components/ui/popover"
import { COURSE_URL } from '@/lib/url';
import TCourse from '@/types/TCourse';

const formSchema = z.object({
  name: z.string().min(1).max(200),
  description: z.string().max(2000),
  studyLoad: z.string().max(30),
  level: z.string(),
  courseLengthInDays: z.string().max(365),
  startDate: z.date(),
  coordinators: z.string(),
})

const CourseEditPage = ({ params }: { params: { id: string } }) => {

    const { toast } = useToast()
    const [ course, setCourse ] = useState<TCourse>()

    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema),
    })

    useEffect(() => {
        getCourseData()
        return () => {}
    }, [params.id])

    const getCourseData = async () => {
        const response = await axios({
            method: "get",
            headers: defaultHeader,
            url: `${COURSE_URL}${params.id}`,
        }).then(response => response).catch(err => console.log(err))
        if (response?.status !== 200) return;
        const data = response.data.course as TCourse
        setCourse(prev => prev = data)
    }

    const onSubmit = (values: z.infer<typeof formSchema>) => {
        axios.put(`${COURSE_URL}${params.id}`, {
            ...values,
            studyLoad: parseInt(values.studyLoad),
            courseLengthInDays: parseInt(values.courseLengthInDays),
        }, {
            headers: defaultHeader
        })
        .then(response => {
            if (response.status === 200) {
                toast({
                    title: "Kursus edukalt muudetud",
                })
            }
        }).catch(err => {
            toast({
                title: "Muutmisel tekkis viga",
            })
        })
    }

    if (course == null) return <>Loading...</>

    return (
        <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
                <FormField
                    defaultValue={course.name}
                    control={form.control}
                    name="name"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Nimi</FormLabel>
                            <FormControl>
                                <Input {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    defaultValue={course.description}
                    control={form.control}
                    name="description"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Kirjeldus</FormLabel>
                            <FormControl>
                                <Input {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    defaultValue={course.studyLoad.toString()}
                    control={form.control}
                    name="studyLoad"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Õppemaht</FormLabel>
                            <FormControl>
                                <Input {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    defaultValue={course.level}
                    control={form.control}
                    name="level"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Õppetase</FormLabel>
                            <FormControl>
                                <Input placeholder='bachelor, master, doctoral' {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    defaultValue={course.courseLengthInDays.toString()}
                    control={form.control}
                    name="courseLengthInDays"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Kursuse pikkus päevades</FormLabel>
                            <FormControl>
                                <Input {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    defaultValue={new Date(course.startDate)}
                    control={form.control}
                    name="startDate"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel className='mr-6'>Alguskuupäev</FormLabel>
                            <FormControl>
                                <Popover>
                                    <PopoverTrigger asChild>
                                        <FormControl>
                                            <Button
                                            variant={"outline"}
                                            className={cn(
                                                "w-[240px] pl-3 text-left font-normal",
                                                !field.value && "text-muted-foreground"
                                            )}
                                            >
                                            {field.value ? (
                                                field.value.toLocaleDateString('et')
                                            ) : (
                                                <span>Pick a date</span>
                                            )}
                                            <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                                            </Button>
                                        </FormControl>
                                    </PopoverTrigger>
                                    <PopoverContent className="w-auto p-0" align="start">
                                        <Calendar
                                            mode="single"
                                            selected={field.value}
                                            onSelect={field.onChange}
                                            disabled={(date) =>
                                                date < new Date() || date < new Date("1900-01-01")
                                            }
                                            initialFocus
                                        />
                                    </PopoverContent>
                                </Popover>
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <FormField
                    defaultValue={course.coordinators}
                    control={form.control}
                    name="coordinators"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Õppejõud</FormLabel>
                            <FormControl>
                                <Input {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                <Button type="submit">Salvesta</Button>
            </form>
        </Form>
    )
}

export default CourseEditPage
