type TCourse = {
    id: number,
    name: string,
    description: string,
    studyLoad: number,
    level: string,
    courseLengthInDays: number,
    startDate: string,
    endDate: string,
    coordinators: string,
}

export default TCourse
